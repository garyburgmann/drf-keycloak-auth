"""Metadata for Authicola package."""
__title__ = 'drf_keycloak_auth'
__description__ = \
    'A convenience libary for authenticating users from Keycloak access tokens'
__url__ = 'https://gitlab.com/garyburgmann/drf-keycloak-auth'
__version__ = '0.0.1.dev3'
__author__ = 'Gary Burgmann'
__author_email__ = 'garyburgmann@gmail.com'
__license__ = 'MIT'
